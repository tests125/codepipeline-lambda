import json
import os
import sys
import boto3
from botocore.exceptions import ClientError


def lambda_handler(event, context):
    dynamodb = boto3.resource('dynamodb', region_name='us-east-1')

    # check if the right http method os used, if not return Forbidden
    if event['httpMethod'] != 'POST':
        return {
            'statusCode': 403,
            'body': json.dumps('Forbidden')
        }

    # transform http body into a json_map
    json_map = json.loads(event['body'])

    # execute create query on dynamodb table
    table = dynamodb.Table('users')
    try:
        response = table.put_item(
            Item={
                'id': json_map['id'],
                'name': json_map['name'],
                'email': json_map['email'],
            }
        )
    except:
        print("-------------------ERROR-------------------")
        return {
            'statusCode': 500,
            'body': 'Internal Server Error'
        }

    # if dynamodb could create the record, we will receive a 200 status code
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        print("-------------------SUCCESS-------------------")
        return {
            'statusCode': 200,
            'body': 'User created'
        }
    else:
        print("-------------------ERROR-------------------")
        return {
            'statusCode': 500,
            'body': 'Internal Server Error'
        }
