import boto3
from moto import mock_dynamodb
from functions.create import lambda_handler


@mock_dynamodb
def test_create_user():
    conn = boto3.resource('dynamodb', region_name='us-east-1')
    conn.create_table(
        TableName='users',
        KeySchema=[
            {
                'AttributeName': 'id',
                'KeyType': 'HASH'
            }
        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'id',
                'AttributeType': 'S'
            },
        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 5
        }
    )
    event = {
        'httpMethod': 'POST',
        'body': '{"id": "1", "name": "John Doe", "email": "hello@hello.com"}'
    }
    context = {}
    response = lambda_handler(event, context)
    print(response)
    assert response['statusCode'] == 200
    assert response['body'] == 'User created'


@mock_dynamodb
def test_failing_create_user():
    conn = boto3.resource('dynamodb', region_name='us-east-1')
    conn.create_table(
        TableName='users',
        KeySchema=[
            {
                'AttributeName': 'id',
                'KeyType': 'HASH'
            }
        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'id',
                'AttributeType': 'S'
            },
        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 5
        }
    )
    event = {
        'httpMethod': 'POST',
        'body': '{"id": "1", "email": "hello@hello.com"}'
    }
    context = {}
    response = lambda_handler(event, context)
    assert response['statusCode'] == 500
    assert response['body'] == 'Internal Server Error'
